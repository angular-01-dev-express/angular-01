import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnimalsComponent } from '../animals/animals.component';
import { NotFound404Component } from '../not-found404/not-found404.component';
import { AppComponent } from '../app.component';
import { IndexComponent } from '../index/index.component';

const routes: Routes = [{
  path: '',
  component: IndexComponent
}, {
  path: 'animals',
  component: AnimalsComponent
}, {
  path: '**',
  component: NotFound404Component
}];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
