import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AccordionService } from '../accordion.service';

@Component({
  selector: 'app-accordion-panel',
  templateUrl: './accordion-panel.component.html',
  styleUrls: ['./accordion-panel.component.scss']
})
export class AccordionPanelComponent implements OnInit {
  @Input('title') title: string;
  @Output() toggle: EventEmitter<any> = new EventEmitter<any>();

  isOpen = false;

  constructor(private accordionService: AccordionService) {
    this.accordionService.increase();
  }

  ngOnInit() {
  }

  getIsOpen() {
    return this.isOpen;
  }
}
