import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AccordionService {
  totalPanels = 0;

  constructor() { }

  increase() {
    this.totalPanels += 1;
  }

  getTotal() {
    return this.totalPanels;
  }
}
