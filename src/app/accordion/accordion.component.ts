import { Component, OnInit, Inject, ContentChildren, QueryList } from '@angular/core';
import { Animal } from '../animal';
import { AccordionService } from '../accordion.service';
import { AccordionPanelComponent } from '../accordion-panel/accordion-panel.component';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {
  @ContentChildren(AccordionPanelComponent) panels: QueryList<AccordionPanelComponent>;

  constructor(private accordionService: AccordionService) {

  }

  ngOnInit() {
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterContentInit() {
    const [firstPanel] = this.panels.toArray();
    firstPanel.isOpen = true;

    this.panels.toArray().forEach((panel: AccordionPanelComponent) => {
      panel.toggle.subscribe(() => {
        this.closeAllPanels();
        panel.isOpen = true;
      });
    });
  }

  closeAllPanels() {
    this.panels.toArray().forEach(panel => panel.isOpen = false);
  }

  getTotalPanels() {
    return this.accordionService.getTotal();
  }
}
