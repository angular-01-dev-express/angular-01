import { Component, OnInit } from '@angular/core';
import { Animal } from '../animal';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  animals: Animal[] = [
    { name: 'Dog', description: 'Lorem ipsum sit dolo amet' },
    { name: 'Cat', description: 'Lorem ipsum sit dolo amet' },
    { name: 'Horse', description: 'Lorem ipsum sit dolo amet' }
  ];

  animal: Animal = {
    name: '', description: ''
  };

  constructor() { }

  ngOnInit() {
  }

}
