import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { FooterComponent } from './footer/footer.component';
import { CubeComponent } from './cube/cube.component';
import { AnimalsComponent } from './animals/animals.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { NotFound404Component } from './not-found404/not-found404.component';
import { IndexComponent } from './index/index.component';
import { AccordionComponent } from './accordion/accordion.component';
import { AccordionPanelComponent } from './accordion-panel/accordion-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    FooterComponent,
    CubeComponent,
    AnimalsComponent,
    NotFound404Component,
    IndexComponent,
    AccordionComponent,
    AccordionPanelComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
